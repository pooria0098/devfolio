from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Q
from django.contrib.postgres.search import TrigramSimilarity
from django.db.models.functions import Greatest
from django.views.generic import CreateView

from blog.models import *
from .forms import ContactUsForm, CommentForm, SearchForm


# def handleIndexPage(request):
#     if request.method == 'POST':
#         contact_form = ContactUsForm(request.POST)
#         if contact_form.is_valid():
#             contact_form.save()
#             return redirect('IndexView')
#
#     contact_form = ContactUsForm()
#
#     context = {
#         'about_Me': get_about_me(),
#         'my_skills': get_my_skills(),
#         'my_services': get_my_services(),
#         'my_reputations': get_my_reputations(),
#         'my_projects': get_my_projects(),
#         'my_testimonials': get_my_testimonials(),
#         'all_articles': get_all_articles(),
#         'contact_form': contact_form,
#     }
#
#     return render(request, 'blog/index.html', context)


class IndexPageView(CreateView):
    form_class = ContactUsForm
    template_name = 'blog/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexPageView, self).get_context_data(**kwargs)
        context.update({
            'about_Me': get_about_me(),
            'my_skills': get_my_skills(),
            'my_services': get_my_services(),
            'my_reputations': get_my_reputations(),
            'my_projects': get_my_projects(),
            'my_testimonials': get_my_testimonials(),
            'all_articles': get_all_articles()
        })
        return context


def blogSingleView(request, pk):
    latest_articles = get_all_articles()[:5]
    specific_article = get_object_or_404(Article, pk=pk)
    specific_comment = get_specific_comments(specific_article)

    search_response = get_all_articles()
    if request.method == 'GET':
        if 'search_field' in request.GET:
            search_form = SearchForm(request.GET)
            if search_form.is_valid():
                valid_search_field = search_form.cleaned_data['search_field']
                search_response = search_response.annotate(similarity=Greatest(
                    TrigramSimilarity('title', valid_search_field),
                    TrigramSimilarity('content', valid_search_field),
                    TrigramSimilarity('short_description', valid_search_field),
                )
                ).filter(similarity__gt=0.3).order_by('-similarity')
                try:
                    specific_article = search_response[0]
                except:
                    pass
                # print(specific_article.pk)
                return redirect('BlogSingleView', pk=specific_article.pk)
    search_form = SearchForm()

    # print(request.POST)
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            name = comment_form.cleaned_data['name']
            email = comment_form.cleaned_data['email']
            content = comment_form.cleaned_data['content']
            gender = comment_form.cleaned_data['gender']
            comment = CommentUs.objects.create(
                name=name, email=email, content=content, article=specific_article, gender=gender)
            comment.save()
            return redirect('BlogSingleView', pk=pk)
    comment_form = CommentForm()

    context = {
        'specific_article': specific_article,
        'comment_form': comment_form,
        'specific_comment': specific_comment,
        'all_articles': latest_articles,
        'search_form': search_form,
    }

    return render(request, 'blog/blog-single.html', context)


def handle_archive_month(request, month):
    search_response = get_specific_articles(month)

    if 'search_field' in request.GET:
        search_form = SearchForm(request.GET)
        if search_form.is_valid():
            valid_search_field = search_form.cleaned_data['search_field']
            # search_response = search_response.filter(
            #     Q(title__icontains=valid_search_field) |
            #     Q(content__in=valid_search_field) |
            #     Q(short_description__in=valid_search_field)
            # )
            search_response = search_response.annotate(similarity=Greatest(
                TrigramSimilarity('title', valid_search_field),
                TrigramSimilarity('content', valid_search_field),
                TrigramSimilarity('short_description', valid_search_field),
            )
            ).filter(similarity__gt=0.3).order_by('-similarity')
    search_form = SearchForm()

    # print(search_response)
    context = {
        'search_response': search_response,
        'search_form': search_form,
    }
    return render(request, 'blog/archive.html', context)
