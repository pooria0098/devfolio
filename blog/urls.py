from django.urls import path
from blog.views import *

urlpatterns = [
    # path('', handleIndexPage, name='IndexView'),
    path('', IndexPageView.as_view(), name='IndexView'),
    path('article/<int:pk>/', blogSingleView, name='BlogSingleView'),
    path('article/archive_in_month/<int:month>/', handle_archive_month, name='ArchiveMonth'),
]
