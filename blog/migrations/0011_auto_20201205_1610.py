# Generated by Django 3.1.4 on 2020-12-05 12:40

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0010_auto_20201205_1609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='created_at',
            field=models.DateTimeField(verbose_name=datetime.datetime(2020, 12, 5, 12, 40, 56, 200222, tzinfo=utc)),
        ),
    ]
