# Generated by Django 3.1.4 on 2020-12-06 10:17

import ckeditor.fields
import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0024_auto_20201206_1333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='created_at',
            field=models.DateTimeField(verbose_name=datetime.datetime(2020, 12, 6, 10, 17, 18, 536932, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='reply',
            name='content',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
