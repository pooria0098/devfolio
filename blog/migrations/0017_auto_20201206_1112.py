# Generated by Django 3.1.4 on 2020-12-06 07:42

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0016_auto_20201205_1933'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='commentus',
            name='active',
        ),
        migrations.RemoveField(
            model_name='commentus',
            name='parent',
        ),
        migrations.CreateModel(
            name='Reply',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('reply', ckeditor.fields.RichTextField()),
                ('comment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='replies', to='blog.commentus')),
            ],
        ),
    ]
