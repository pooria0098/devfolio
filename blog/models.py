from PIL import Image
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class AboutMe(models.Model):
    userprofile = models.OneToOneField('UserProfile', on_delete=models.CASCADE)
    phoneNumber = models.CharField(max_length=11)
    description = RichTextField(blank=True)

    def __str__(self):
        return f'{self.userprofile.user.first_name}'

    class Meta:
        verbose_name_plural = 'AboutMe'


def get_about_me():
    if AboutMe.objects.filter(userprofile__user__first_name__exact='پوریا').exists():
        return AboutMe.objects.get(userprofile__user__first_name__exact='پوریا')
    else:
        return None


class MySkills(models.Model):
    name = models.CharField(max_length=50)
    percentages = models.IntegerField()

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name_plural = 'MySkills'


def get_my_skills():
    return MySkills.objects.all()


class Services(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    icon = models.CharField(max_length=200)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = 'Services'


def get_my_services():
    return Services.objects.all()


class Reputations(models.Model):
    name = models.CharField(max_length=50)
    number = models.IntegerField()
    icon = models.CharField(max_length=200)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = 'Reputations'


def get_my_reputations():
    return Reputations.objects.all()


class Category(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % self.title

    class Meta:
        verbose_name_plural = 'Categories'


class Projects(models.Model):
    title = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    done_at = models.DateTimeField(auto_now_add=True)
    img = models.ImageField(upload_to='images/', default='')
    url = models.URLField(null=True, blank=True)

    def __str__(self):
        return '%s' % self.title

    class Meta:
        verbose_name_plural = 'Projects'


def get_my_projects():
    return Projects.objects.all()


class Testimonials(models.Model):
    userprofile = models.OneToOneField('UserProfile', on_delete=models.CASCADE)
    description = models.TextField()

    def __str__(self):
        return self.userprofile.user.first_name

    class Meta:
        verbose_name_plural = 'Testimonials'


def get_my_testimonials():
    return Testimonials.objects.all()


class UserProfile(models.Model):
    avatar = models.ImageField(upload_to='images/', default='')
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.avatar.path)
        if img.height > 400 or img.width > 400:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.avatar.path)

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name

    class Meta:
        verbose_name_plural = 'UserProfiles'


class Article(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    cover = models.ImageField(upload_to='images/', default='')
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    created_at = models.DateTimeField(timezone.now())
    updated_at = models.DateTimeField(auto_now=True)
    content = RichTextUploadingField(blank=True)
    short_description = RichTextField()
    time_to_read = models.CharField(max_length=30)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Articles'


def get_all_articles():
    return Article.objects.all().order_by('-created_at')


def get_specific_articles(args):
    return Article.objects.filter(created_at__month=args)


class CommentUs(models.Model):
    STATUS_CHOICES = (
        ('male', 'مرد'),
        ('female', 'زن'),
    )
    name = models.CharField(max_length=50)
    email = models.EmailField()
    content = RichTextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    gender = models.CharField(max_length=6, choices=STATUS_CHOICES)

    def __str__(self):
        return 'Comment by ' + self.name

    class Meta:
        verbose_name_plural = 'Comments'
        ordering = ('-created_at',)


def get_specific_comments(args):
    return CommentUs.objects.filter(article__title=args)


# class Reply(models.Model):
#     STATUS_CHOICES = (
#         ('male', 'male'),
#         ('female', 'female'),
#     )
#     commentUs = models.ForeignKey(CommentUs, on_delete=models.CASCADE)
#     name = models.CharField(max_length=50)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#     content = models.TextField()
#     gender = models.CharField(max_length=6,default='male')
#
#     class Meta:
#         verbose_name_plural = 'Replies'
#         ordering = ('-created_at',)
#
#     def __str__(self):
#         return 'Replied by ' + self.name


class ContactUs(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    subject = models.CharField(max_length=50, blank=True)
    content = RichTextField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'ContactUs'
