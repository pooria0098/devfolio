from django.contrib import admin
from blog import models

admin.site.register(models.AboutMe)
admin.site.register(models.Testimonials)
admin.site.register(models.Category)
admin.site.register(models.UserProfile)


@admin.register(models.ContactUs)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'subject')


@admin.register(models.Projects)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('title', 'category')


@admin.register(models.Reputations)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'number')


@admin.register(models.Services)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


@admin.register(models.MySkills)
class SkillAdmin(admin.ModelAdmin):
    list_display = ('name', 'percentages')
    ordering = ('-percentages',)


class CommentInstanceInline(admin.TabularInline):
    model = models.CommentUs
    extra = 1
    classes = ['collapse']


@admin.register(models.Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'author', 'created_at', 'time_to_read')
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None, {'fields': ('title', 'slug', 'category')}),
        ('Content',
         {'fields': ('short_description', 'content'), 'classes': ['collapse']}),
        ('ExtraFeatures', {'fields': ('author', 'cover'), 'classes': ['collapse']}),
        ('DateTime', {'fields': ('time_to_read', 'created_at'), 'classes': ['collapse']}),
    )
    inlines = [CommentInstanceInline]
