from django import forms
from .models import ContactUs, CommentUs


class ContactUsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update(
            {'class': 'form-control',
             'placeholder': 'نام شما',
             'data-rule': 'minlen:4',
             'data-msg': 'لطفا بیشتر از 4 کاراکتر وارد کنید'}
        )
        self.fields['email'].widget.attrs.update(
            {'class': 'form-control',
             'placeholder': 'ایمیل شما',
             'data-rule': 'email',
             'data-msg': 'لطفا ایمیل معتبر وارد کنید'}
        )
        self.fields['subject'].widget.attrs.update(
            {'class': 'form-control',
             'placeholder': 'موضوع کنید',
             'data-rule': 'minlen:4',
             'data-msg': 'لطفا بیشتر از 8 کاراکتر وارد کنید'}
        )
        self.fields['content'].widget.attrs.update(
            {'class': 'form-control',
             'placeholder': 'پیام',
             'data-rule': 'required',
             'rows': '5',
             'data-msg': 'پیام خود را برا ی ما بنویسید'}
        )

    class Meta:
        model = ContactUs
        fields = '__all__'
        widgets = {
            'email': forms.EmailInput(),
            # 'content': forms.TextInput(),
        }


class CommentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update(
            {'class': 'form-control input-mf',
             'placeholder': 'نام *',
             'required': 'required'
             }
        )
        self.fields['email'].widget.attrs.update(
            {'class': 'form-control input-mf',
             'placeholder': 'ایمیل *',
             'required': 'required'}
        )
        self.fields['content'].widget.attrs.update(
            {'class': 'form-control input-mf',
             'placeholder': 'نظر *',
             'required': 'required',
             'type': 'textarea'}
        )

    class Meta:
        model = CommentUs
        fields = ['name', 'email', 'content', 'gender']
        widgets = {
            'email': forms.EmailInput(),
            'content': forms.TextInput(),
        }


class SearchForm(forms.Form):
    search_field = forms.CharField(max_length=10)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['search_field'].widget.attrs.update(
            {'class': 'form-control input-mf',
             'placeholder': 'جستجو برای ...',
             'aria-label': 'Search for...',
             'label': '',
             }
        )
