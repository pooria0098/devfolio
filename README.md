personal weblog<br>
Aparat Link for view Project : https://www.aparat.com/v/erD7i
<hr/>
<hr/>
Installation
<hr/>
1) <b>clone</b> or <b>download</b> this project.<br/>
2) you have two choice :
one : change DB from PostgreSQL to SQLite3 so you don't need any change in .env
two : create PostgreSQL and put name , user and pass in .env file
2) You need change name of .env_sample to .env and put configuration with default values.<br/>
3) <code>python manage.py makemigrations</code> and <code>python manage.py migrate</code> for create DB<br/>
4) At last you can Run project with <code>python manage.py runserver</code>
